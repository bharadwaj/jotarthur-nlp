#!/usr/bin/python

from nltk.corpus.reader import CategorizedPlaintextCorpusReader
from featx import simple_label_feats_from_corpus, simple_split_label_feats, bag_of_words
from ling_parser import sent_tagger, out_chunks, nltk_ne_chunks, get_continuous_chunks, conll_tag_chunks, get_ne_lists
from nltk.classify import NaiveBayesClassifier
from nltk import word_tokenize
from nltk.classify.util import accuracy


def ling_processor(user_sentence):

	print "Hello, I am Arthur!"
	#user_sentence = read_input()

	commerce_corpus = load_corpus()
	nb_classifier, test_feats = init_classifier_naive_bayes(commerce_corpus)

	entities = entity_recognizer(user_sentence)
	classification = classify_sent_naive_bayes(nb_classifier, test_feats, user_sentence)

	response_dict = {'classification': classification, 'entities': entities}
	#response_dict = {'classification': classification}
	return response_dict

def read_input():
    #Change it to user typing.
    fname = "./ling/commerce_test.txt"
    with open(fname) as f:
    	content = f.readlines()

    i = 7
    return content[i]

def load_corpus():
	reader = CategorizedPlaintextCorpusReader('./ling', r'commerce_.*\.txt', 
		cat_map={'commerce_buy.txt': ['buy'], 'commerce_question.txt': ['question']})

	return reader

def init_classifier_naive_bayes(corpus):
	lfeats = simple_label_feats_from_corpus(corpus)
	train_feats, test_feats = simple_split_label_feats(lfeats, split=0.75)
	return NaiveBayesClassifier.train(train_feats), test_feats


def classify_sent_naive_bayes(nb_classifier, test_feats, sentence):
	#For actual sentence String inputs.
	with_feats = bag_of_words(word_tokenize(sentence))
	#with_feats = bag_of_words(sentence)

	print "Cassified to: ", nb_classifier.classify(with_feats)
	print "With an accuracy of: ", accuracy(nb_classifier, test_feats)*100
	#print nb_classifier.show_most_informative_features(5)
	#print test_feats[1]
	print "Number of test sets: ", len(test_feats)

	return nb_classifier.classify(with_feats)


def entity_recognizer(sentence):
	tagged_sent = sent_tagger(word_tokenize(sentence))

	#Custom Regex Chunker
	#chunked_sent = out_chunks(tagged_sent)
	print sentence
	print "from ne_chunk:"
	#print get_continuous_chunks(sentence)
	#print nltk_ne_chunks(tagged_sent)
	#print type(conll_tag_chunks(nltk_ne_chunks(tagged_sent)))
	#print type(get_ne_lists(tagged_sent))
	#print get_ne_lists(tagged_sent)

	return get_ne_lists(tagged_sent)
