import nltk

sent = "get me Ibanez headphones"

words_sent = nltk.word_tokenize(sent)
pos_sent = nltk.pos_tag(words_sent)

print pos_sent

grammar = "NP: {<NNP>?<NNS>?<NN> | <NN> }"

cp = nltk.RegexpParser(grammar)

result = cp.parse(pos_sent)

print(result)

result.draw()
