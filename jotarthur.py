#!/usr/bin/python

import ling_processor as ling_p
from flask import Flask, request, json, Response, jsonify


app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/text', methods = ['POST'])
def text_process():
	if request.headers['Content-Type'] == 'application/json':

		user_sentence = request.json.get('text')
		resp_dict = ling_p.ling_processor(user_sentence)
		
		resp = jsonify(resp_dict)
		resp.status_code = 200

		return resp
	

	return 'Unknown Access Point'



def main():
    if __name__ == "__main__":
                 main()

