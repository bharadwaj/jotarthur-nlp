import nltk
from nltk.chunk import RegexpParser
from nltk.chunk.util import tree2conlltags, conlltags2tree
from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree

def get_ne_lists(tagged_sentence):
	#namedEnt = nltk.ne_chunk(tagged_sentence, binary=True)
	#tree = ne_chunk(tagged_sentence)
	tree = ne_chunk(tagged_sentence, binary=True)
	#sub_leaves(tree, 'PERSON')
	#sub_leaves(tree, 'ORGANIZATION')

	#All the NE tagged sentence list
	#sub_leaves(ne_chunk(tagged_sentence, binary=True), 'NE')

	#namedEnt.draw()
	grammar = "NP: {<DT>?<JJ>*<NN>}"
	grammar2 = "NP: {<DT><NNP.*><.*>*<NN.*>}}<VB.*>{" 
	cp = RegexpParser(grammar)
	print cp.parse(tagged_sentence)
	#print list(tails(cp.parse(tagged_sentence)))
	return sub_leaves(tree, 'NE')

'''
===============================
Extracting Proper Noun Entities
===============================
chunker = RegexpParser(r"
NAME:
	{<NNP>+}
"")
>>> sub_leaves(chunker.parse(treebank_chunk.tagged_sents()[0]), 'NAME')
[[('Pierre', 'NNP'), ('Vinken', 'NNP')], [('Nov.', 'NNP')]]
'''

def out_chunks(tagged_sentence):
	chunker = RegexpParser(r'''
		NP:
				{<DT><NN.*><.*>*<NN.*>}
				}<VB.*>{
		''')
	grammar = "NP: {<DT>?<JJ>*<NN>}" 
	cp = RegexpParser(grammar)
	#print cp.parse(tagged_sentence)

def nltk_ne_chunks(tagged_sentence):
	#namedEnt = nltk.ne_chunk(tagged_sentence, binary=True)
	namedEnt = nltk.ne_chunk(tagged_sentence)

	#namedEnt.draw()
	return namedEnt

def get_continuous_chunks(text):
    chunked = nltk.ne_chunk(pos_tag(word_tokenize(text)))
    prev = None
    continuous_chunk = []
    current_chunk = []

    for i in chunked:
        if type(i) == Tree:
            current_chunk.append(" ".join([token for token, pos in i.leaves()]))
        elif current_chunk:
            named_entity = " ".join(current_chunk)
            if named_entity not in continuous_chunk:
                continuous_chunk.append(named_entity)
                current_chunk = []
        else:
            continue

    return continuous_chunk

def sent_tagger(sentence):

	#text = word_tokenize(sentence)
	return nltk.pos_tag(sentence)

def sub_leaves(tree, label):
	return [t.leaves() for t in tree.subtrees(lambda s: s.label() == label)]

def conll_tag_chunks(chunk_sents):
	'''Convert each chunked sentence to list of (tag, chunk_tag) tuples,
	so the final result is a list of lists of (tag, chunk_tag) tuples.
	>>> from nltk.tree import Tree
	>>> t = Tree('S', [Tree('NP', [('the', 'DT'), ('book', 'NN')])])
	>>> conll_tag_chunks([t])
	[[('DT', 'B-NP'), ('NN', 'I-NP')]]
	'''
	tagged_sents = [tree2conlltags(tree) for tree in chunk_sents]
	return [[(t, c) for (w, t, c) in sent] for sent in tagged_sents]

